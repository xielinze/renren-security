package io.renren.modules.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.common.utils.PageUtils;
import io.renren.modules.sys.entity.TestOrderEntity;

import java.util.Map;

/**
 * 
 *
 * @author Mark
 * @email sunlightcs@gmail.com
 * @date 2019-03-21 15:07:49
 */
public interface TestOrderService extends IService<TestOrderEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

