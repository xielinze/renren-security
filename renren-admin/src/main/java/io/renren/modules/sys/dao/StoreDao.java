package io.renren.modules.sys.dao;

import io.renren.modules.sys.entity.StoreEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author Mark
 * @email sunlightcs@gmail.com
 * @date 2019-03-21 13:24:24
 */
@Mapper
public interface StoreDao extends BaseMapper<StoreEntity> {
	
}
