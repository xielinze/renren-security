package io.renren.modules.sys.dao;

import io.renren.modules.sys.entity.TestOrderEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author Mark
 * @email sunlightcs@gmail.com
 * @date 2019-03-21 15:07:49
 */
@Mapper
public interface TestOrderDao extends BaseMapper<TestOrderEntity> {
	
}
