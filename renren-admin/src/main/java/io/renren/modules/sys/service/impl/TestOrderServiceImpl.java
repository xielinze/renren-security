package io.renren.modules.sys.service.impl;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;

import io.renren.modules.sys.dao.TestOrderDao;
import io.renren.modules.sys.entity.TestOrderEntity;
import io.renren.modules.sys.service.TestOrderService;


@Service("testOrderService")
public class TestOrderServiceImpl extends ServiceImpl<TestOrderDao, TestOrderEntity> implements TestOrderService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {

        String status = (String)params.get("status");

        IPage<TestOrderEntity> page = this.page(
                new Query<TestOrderEntity>().getPage(params),
                new QueryWrapper<TestOrderEntity>().eq(StringUtils.isNotBlank(status),"status",status)
        );

        return new PageUtils(page);
    }

}
