package io.renren.modules.sys.controller;

import java.util.Arrays;
import java.util.Map;

import io.renren.common.validator.ValidatorUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.renren.modules.sys.entity.TestOrderEntity;
import io.renren.modules.sys.service.TestOrderService;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;



/**
 * 
 *
 * @author Mark
 * @email sunlightcs@gmail.com
 * @date 2019-03-21 15:07:49
 */
@RestController
@RequestMapping("sys/testorder")
public class TestOrderController {
    @Autowired
    private TestOrderService testOrderService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("sys:testorder:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = testOrderService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("sys:testorder:info")
    public R info(@PathVariable("id") Integer id){
        TestOrderEntity testOrder = testOrderService.getById(id);

        return R.ok().put("testOrder", testOrder);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("sys:testorder:save")
    public R save(@RequestBody TestOrderEntity testOrder){
        testOrderService.save(testOrder);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("sys:testorder:update")
    public R update(@RequestBody TestOrderEntity testOrder){
        ValidatorUtils.validateEntity(testOrder);
        testOrderService.updateById(testOrder);
        
        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("sys:testorder:delete")
    public R delete(@RequestBody Integer[] ids){
        testOrderService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
